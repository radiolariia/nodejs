class UniversalValidator {
    isEmpty = value => {
        return value === '' ? true : false;
    }

    isString = value => {
        return /^[a-zA-Z]+$/g.test(value);
    }    

    isEmail = value => {
        return /^[a-z0-9]*[\w.+\-]*@gmail\.com$/gi.test(value);
    };

    isPhoneNumber = value => {
        return /^\+380(\d{9})$/g.test(value);
    };

    isNumber = value => {
        return /^\d+$/g.test(value);
    }

    isAppropriateNumber = (value, max, min) => {
        const resultVal = Number(value); 
        if(min === undefined) {
            return resultVal < max;
        } else {
            return resultVal >= min && resultVal <= max;
        }
    }
    isAppropriateLength = (value, min, max) => { 
        if(max) {
            return value.length >= min && value.length <= max;
        } else {
            return value.length >= min;
        }
    }
    hasIdField = value => {
        return value !== undefined;
    }
    hasUnwantedFields = (value, length) => {
        return Object.keys(value).length > length;
    }
}

function checkIsObjEmpty(obj) {
    for(let key in obj) {
        if(obj.hasOwnProperty(key)) return false;
    }
    return true;
}

exports.UniversalValidator = new UniversalValidator();
exports.checkIsObjEmpty = checkIsObjEmpty;