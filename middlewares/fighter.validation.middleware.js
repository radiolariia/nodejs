const { fighter } = require('../models/fighter');
const { UniversalValidator, checkIsObjEmpty } = require('./UniversalValidator');

const validateFighterQuery = fighter => {
    let errors = {};

    fighter.name = fighter.hasOwnProperty('name') ? fighter.name : '';
    fighter.health = fighter.hasOwnProperty('health') ? fighter.health : '';
    fighter.power = fighter.hasOwnProperty('power') ? fighter.power : '';
    fighter.defense = fighter.hasOwnProperty('defense')  ? fighter.defense : '';

    for(let field in fighter) {
        if(UniversalValidator.isEmpty(fighter[field])) {
            errors[field] = `Enter ${field}, please`;
        } 
    }

    if (!UniversalValidator.isNumber(fighter.health)) {
        errors.health = 'Health should be a number';
    }
    
    if (!UniversalValidator.isAppropriateNumber(fighter.health, 100, 1)) {
        errors.health = 'Health should be in range 1 to 100';
    }

    if (!UniversalValidator.isNumber(fighter.power)) {
        errors.power = 'Power should be a number';
    }
  
    if (!UniversalValidator.isAppropriateNumber(fighter.power, 100, 1)) {
        errors.power = 'Power should be in range 1 to 100';
    }

    if (!UniversalValidator.isNumber(fighter.defense)) {
        errors.defense = 'Defense should be a number';
    }

    if (!UniversalValidator.isAppropriateNumber(fighter.defense, 10, 1)) {
        errors.defense = 'Defense should be in range [1, 10]';
    }

    if(UniversalValidator.hasUnwantedFields(fighter, 4)) {
        errors.fields = 'Exclude unnecessary fields, prankster!'
    }

    return {
        errors,
        isValid: checkIsObjEmpty(errors),
    };
};


const createFighterValid = (req, res, next) => {
    const { errors, isValid } = validateFighterQuery(req.body);	

    if (!isValid) {
    return res.status(400).json({
        error: true,
        message: Object.values(errors),
    });
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    const { errors, isValid } = validateFighterQuery(req.body);	
    
    if (!isValid) {
    return res.status(400).json({
        error: true,
        message: Object.values(errors),
    });
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;