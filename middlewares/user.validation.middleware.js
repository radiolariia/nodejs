const { user } = require('../models/user');
const { UniversalValidator, checkIsObjEmpty } = require('./UniversalValidator');

const validateUserQuery = user => {
    let errors = {};

    user.firstName =  user.hasOwnProperty('firstName')  ? user.firstName : '';
    user.lastName = user.hasOwnProperty('lastName') ? user.lastName : '';
    user.email = user.hasOwnProperty('email') ? user.email : '';
    user.phoneNumber = user.hasOwnProperty('phoneNumber') ? user.phoneNumber : '';
    user.password = user.hasOwnProperty('password') ? user.password : '';

    for(let field in user) {
        if(UniversalValidator.isEmpty(user.field)) {
            errors[field] = `Enter ${field}, please`;
        } 
    }
    
    if(!UniversalValidator.isString(user.firstName) ||
        !UniversalValidator.isString(user.lastName)
    ) {
        errors.name = 'Enter your real name, please';
    }

    if(!UniversalValidator.isEmail(user.email)) {
        errors.email = 'Enter valid gmail adress, please';
    }

    if(!UniversalValidator.isPhoneNumber(user.phoneNumber)) {
        errors.phoneNumber = 'Enter phone number in format +380123456789, please';
    }

    if(!UniversalValidator.isAppropriateLength(user.password, 3)) {
        errors.password = 'Enter password with minimum 3 symbols, please';
    }

    if(UniversalValidator.hasUnwantedFields(user, 5)) {
        errors.fields = 'Exclude unnecessary fields, prankster!'
    }

    return {
        errors,
        isValid: checkIsObjEmpty(errors),
    }
}

const createUserValid = (req, res, next) => {
    const { errors, isValid } = validateUserQuery(req.body);	
    
    if (!isValid) {
    return res.status(400).json({
        error: true,
        message: Object.values(errors),
    });
    }
    next();
}

const updateUserValid = (req, res, next) => {
    const { errors, isValid } = validateUserQuery(req.body);	
    
    if (!isValid) {
    return res.status(400).json({
        error: true,
        message: Object.values(errors),
    });
    }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;