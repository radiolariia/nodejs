const { UserRepository } = require('../repositories/userRepository');

class UserService {
    getOneUser(search) {
        const user = UserRepository.getOne(search);
        console.log(user)
        if (!user) {
          return null;
        }
        return user;
    }
    getUsers() {
      const users = UserRepository.getAll();
      if (!users) {
        return null;
      }
      return users;
    }    
    create(user) {
        const userInDB = UserRepository.getOne({ phoneNumber: user.phoneNumber }) ||
                          UserRepository.getOne({ email: user.email })
        if(userInDB !== undefined) {
          return null;
        }
        return UserRepository.create(user);
      } 
    update(id, data) {
      if (UserRepository.getOne({ id }) !== undefined) {
        const updatedUser = UserRepository.update(id, data);
        return updatedUser;
      }
      return null;
    }  
    delete(id) {
      if (UserRepository.getOne({ id }) !== undefined) {
        const deletedUser = UserRepository.delete(id);
        return deletedUser;
      }
      return null;
    } 
}

module.exports = new UserService();

