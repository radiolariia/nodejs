const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getOneFighter(search) {
        const fighter = FighterRepository.getOne(search);
        if (!fighter) {
          return null;
        }
        return fighter;
    }
    getFighters() {
      const fighters = FighterRepository.getAll();
      if (!fighters) {
        return null;
      }
      return fighters;
    }    
    create(fighter) {
      const fighterInDB = FighterRepository.getOne({ name: fighter.name });

      if(fighterInDB !== undefined) {
        return null;
      }
      return FighterRepository.create(fighter);
    } 
    update(id, data) {
      const fighterInDB = FighterRepository.getOne({ name: fighter.name }) ||
                          FighterRepository.getOne({ id: fighter.id });
      if(fighterInDB !== undefined ) {
        const updatedfighter = FighterRepository.update(id, data);
        return updatedfighter;
      }
      return null;
    }  
    delete(id) {
      if(FighterRepository.getOne({ id }) !== undefined) {
        const deletedfighter = FighterRepository.delete(id);
        return deletedfighter;
      }
      return null;
    } 
}

module.exports = new FighterService();