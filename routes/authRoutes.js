const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        const email = req.body.email;
        const password = req.body.password;
        const user = AuthService.login({ email });

        if(!user) {
            res.status(401).send({
                error: true,
                message: 'Can NOT authorize - invalid user data',
            });
        }
        if (password === user.password) {
            res.json(user);
          } else {
            res.status(401).json({
              error: true,
              message: 'Can NOT authorize - invalid user data',
            });
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;