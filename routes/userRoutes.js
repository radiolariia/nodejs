const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { User } = require('../models/user')

const router = Router();

router.get('/', (req, res, next) => {
    const users = UserService.getUsers();

    if(users.length > 0) {
      return res.status(200).json(users);
    } else {
      return res.status(404).json({
        error: true,  
        message: 'There are no users',
    });
    }
}).get('/:id', (req, res, next) => {
    const id = req.params.id;
    const user = UserService.getOneUser({ id });

    if(user) {
        return res.status(200).send(user);
    } else {
        return res.status(404).json({
            error: true,
            message: 'Looks like any user has not such id'
        });
    }
}).post('/', createUserValid, (req, res, next) => {
    const newUser = new User(req.body);
    const result = UserService.create(newUser);
    
    if(result) {
        return res.status(200).send(result);
    } else {
        return res.status(400).json({
            error: true,
            message: 'User with such email or phone number already exists!'
        });
    }
}).put('/:id', updateUserValid, (req, res, next) => {
    const id = req.params.id;
    const userData = req.body;
    const updatedUser = UserService.update(id, userData);

    if (updatedUser) {
        return res.status(200).json(updatedUser);
    } else {
        return res.status(404).json({
            error: true,
            message: 'Such user does NOT exist OR you are trying to type email or phone number of the existing user'
        });
    }
}).delete('/:id', (req, res, next) => {
    const id = req.params.id;
    const userToDelete = UserService.delete(id);
    if (userToDelete) {
        return res.status(200).json('Deleted successfully!');
    } else {
        return res.status(404).json({
            error: true,
            message: 'Such user does NOT exist'
        });
    }
})



module.exports = router;